package com.afkl.cases.df;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan({"com.afkl.travel.services","com.afkl.travel.controller",
"com.afkl.travel.consumers","com.afkl.travel.assembler"})
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static/index.html");
        registry.addResourceHandler("/app.js").addResourceLocations("classpath:/static/app.js");        
        registry.addResourceHandler("/styles.css").addResourceLocations("classpath:/static/styles.css");
        registry.addResourceHandler("/back_image_1.jpg").addResourceLocations("classpath:/static/back_image_1.jpg");
        registry.addResourceHandler("/front_end_services.js").addResourceLocations("classpath:/static/front_end_services.js");        
    }        

}
