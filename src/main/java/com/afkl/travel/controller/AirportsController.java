package com.afkl.travel.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.afkl.travel.dto.AirportsDTO;
import com.afkl.travel.services.AirportsService;

/**
 *
 *  REST service for users.
 *
 */
@Controller
@RequestMapping("/airports")
public class AirportsController {

    private static final Logger LOGGER = Logger.getLogger(AirportsController.class);

    @Autowired
    AirportsService airportsService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public AirportsDTO searchAirports(int size, int page, String lang, String term) {
    		
    	AirportsDTO airports = airportsService.searchAirports(size, page, lang, term);        
    	
    	airports = new AirportsDTO(); 
    	airports.setCurrentPage(555);
    	
    	
    	LOGGER.info(airports.getCurrentPage());
    	
    	
        return airports != null ? airports : null;
    }
    

}
