package com.afkl.travel.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.afkl.travel.dto.AirportsDTO;
import com.afkl.travel.dto.FareDTO;
import com.afkl.travel.services.AirportsService;
import com.afkl.travel.services.FareService;

/**
 *
 *  REST service for users.
 *
 */
@EnableWebMvc
@Controller
@RequestMapping("/fares")
public class FaresController {

    private static final Logger LOGGER = Logger.getLogger(FaresController.class);

    @Autowired
    FareService fareService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public FareDTO findFare(String currency, 
    		@RequestParam(value = "origin_code", required = false) String origin,
    		@RequestParam(value = "destination_code", required = false) String destination) {
    		
    	FareDTO fare = fareService.retrieveFare(currency);        
    	
    	
        return fare != null ? fare : null;
    }
    

}
