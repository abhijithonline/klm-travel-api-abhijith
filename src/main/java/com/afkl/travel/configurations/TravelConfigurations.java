package com.afkl.travel.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * This class will load the XML configurations which are externalized
 * 
 * @author Abhijith
 *
 */

@Configuration  
@ImportResource(value = { 
	    "classpath:travel-config.xml"
	    })  
public class TravelConfigurations {

}