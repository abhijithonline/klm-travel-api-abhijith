package com.afkl.travel.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.afkl.travel.assembler.AirportsAssembler;
import com.afkl.travel.assembler.FaresAssembler;
import com.afkl.travel.consumers.ConsumeOauth2Service;
import com.afkl.travel.dto.AirportsDTO;
import com.afkl.travel.request.TravelRequest;
import com.afkl.travel.response.TravelResponse;

/**
 *
 * Business service for Airport RESTful Consumer related operations
 *
 */
@Service
public class AirportsService {

    private static final Logger LOGGER = Logger.getLogger(AirportsService.class);    

    @Autowired
    ConsumeOauth2Service consumer;
    
    @Autowired
    AirportsAssembler airportAssembler;

    /**
     * 
     * Searches and returns list of airports based on the term
     * 
     * @param size
     * @param page
     * @param lang
     * @param term
     */
    @Transactional
    public AirportsDTO searchAirports(int size, int page, String lang, String term) {
        
    	TravelRequest travelRequest = new TravelRequest();
    	TravelResponse travelResponse = null; //consumer.consumeService(travelRequest);

        if (travelResponse != null) {
        	
        } else {
            LOGGER.info("Airport not available with search term");
        }
        
        AirportsDTO airports = airportAssembler.constructAirports(travelResponse);
        
        return airports;
        
    } 
}
