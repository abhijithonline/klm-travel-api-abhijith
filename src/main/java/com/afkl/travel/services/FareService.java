package com.afkl.travel.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.afkl.travel.assembler.FaresAssembler;
import com.afkl.travel.consumers.ConsumeOauth2Service;
import com.afkl.travel.dto.FareDTO;
import com.afkl.travel.request.TravelRequest;
import com.afkl.travel.response.TravelResponse;

/**
 *
 * Business service for Airport RESTful Consumer related operations
 *
 */
@Service
public class FareService {

    private static final Logger LOGGER = Logger.getLogger(FareService.class);    

    @Autowired
    ConsumeOauth2Service consumer;
    
    @Autowired
    FaresAssembler faresAssembler;

    /**
     * 
     * @param currency
     * @return
     */
    @Transactional
    public FareDTO retrieveFare(String currency) {
        
    	TravelRequest travelRequest = new TravelRequest();
    	TravelResponse travelResponse =  null; //consumer.consumeService(travelRequest);

        if (travelResponse != null) {
        	
        } else {
            LOGGER.info("Airport not available with search term");
        }
        
        FareDTO fares = faresAssembler.constructFares(travelResponse);
        
        return fares;
        
    }	
}
