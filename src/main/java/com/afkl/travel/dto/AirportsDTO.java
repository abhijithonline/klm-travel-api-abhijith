package com.afkl.travel.dto;

import java.util.List;

/**
 *
 * JSON serializable DTO containing data concerning a airport search request.
 *
 */
public class AirportsDTO {

    private int currentPage;
    private int totalPages;
    List<AirportDTO> airports;

    public AirportsDTO(int currentPage, int totalPages, List<AirportDTO> airports) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.airports = airports;
    }
    
    public AirportsDTO(){
    	
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<AirportDTO> getAirports() {
        return airports;
    }

    public void setAirports(List<AirportDTO> airports) {
        this.airports = airports;
    }
}

