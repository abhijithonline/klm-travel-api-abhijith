package com.afkl.travel.usage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;

/**
 * 
 * For service level usage 
 * 
 * For HTTP usage, recommended approach is NGINX metrics
 * 
 * @author Abhijith
 *
 */

public class TravelServiceMetricsAspect {

    /*private final CounterService counterService;

    @Autowired
    public TravelServiceMetricsAspect(CounterService counterService) {
        this.counterService = counterService;
    }

    @AfterReturning(pointcut = "execution(* com.afkl.travel.services.AirportServices.searchAirports && args(number)", argNames = "number")
    public void afterCallingSearchAirports(int number) {
        counterService.increment("counter.calls.search_airports");
        counterService.increment("counter.calls.search_airports." + number);
    }

    @AfterThrowing(pointcut = "execution(* com.afkl.travel.services.FareService.searchFares(int))", throwing = "e")
    public void afterSearchFares()) {
        counterService.increment("counter.errors.searchFares");
    }*/

}