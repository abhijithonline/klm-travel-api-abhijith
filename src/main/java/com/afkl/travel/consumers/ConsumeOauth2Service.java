package com.afkl.travel.consumers;

import java.net.URI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.client.oauth2.ClientIdentifier;
import org.glassfish.jersey.client.oauth2.OAuth2CodeGrantFlow;
import org.glassfish.jersey.client.oauth2.TokenResult;
import org.springframework.stereotype.Service;

import com.afkl.travel.oauth2.SimpleOAuthService;
import com.afkl.travel.request.TravelRequest;
import com.afkl.travel.response.TravelResponse;

/**
 * This class will invoke the RESTful Service to fetch the list of Airports
 * Uses OAUTH2 client approach based on oauth2-client-google-webapp
 * 
 * INCOMPLETE
 * 
 * @author Abhijith
 *
 */
@Service
public class ConsumeOauth2Service {
	
	/*@Context
    private UriInfo uriInfo;
	
	public TravelResponse consumeService (TravelRequest travelRequest){
		
		*//**
		 * Prepare and Initialize the key and secret required for the token
		 *//*
		String consumerKey = System.getProperty("consumerKey");
		String consumerSecret = System.getProperty("consumerSecret");
		
		SimpleOAuthService.setClientIdentifier(new ClientIdentifier(consumerKey, consumerSecret));
        
		 // check access token
        if (SimpleOAuthService.getAccessToken() == null) {
        	//TODO
        	return null;
            //return travelAuthRedirect();
        }
        // We have already an access token. Query the data from Google API.
        final Client client = SimpleOAuthService.getFlow().getAuthorizedClient();
        
        //TODO        
        
        final OAuth2CodeGrantFlow flow = SimpleOAuthService.getFlow();

        //TODO
        final TokenResult tokenResult = flow.finish("code", "state");

        SimpleOAuthService.setAccessToken(tokenResult.getAccessToken());

        // authorization is finished -> now redirect back to the task resource
        final URI uri = UriBuilder.fromUri(uriInfo.getBaseUri()).path("tasks").build();
        
        Response rs = Response.seeOther(uri).build();
        
        return (TravelResponse)rs.getStringHeaders().get(0);	
	}
	
	
	@GET
    @Path("authorize")
    public Response authorize(@QueryParam("code") String code, @QueryParam("state") String state) {
        final OAuth2CodeGrantFlow flow = SimpleOAuthService.getFlow();

        final TokenResult tokenResult = flow.finish(code, state);

        SimpleOAuthService.setAccessToken(tokenResult.getAccessToken());

        // authorization is finished -> now redirect back to the task resource
        final URI uri = UriBuilder.fromUri(uriInfo.getBaseUri()).path("tasks").build();
        return Response.seeOther(uri).build();
    }*/
	
}
