angular.module('travelServices', [])
    .service('AirportsService', function($http, $q) {
        return {
            searchAirports: function(size, page, lang, term) {
                var deferred = $q.defer();

                $http.get('/airports/',{
                    params: {
                        size: size,
                        page: page,
                        lang: lang,                        
                        term: term
                    }
                })
                .then(function (response) {
                	
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error retrieving list of airports');
                    }
                });

                return deferred.promise;
            }
        }
    }).service('FareService', function($http, $q) {
        return {
            searchAirports: function(size, page, lang, term) {
                var deferred = $q.defer();

                $http.get('/fares/',{
                    params: {
                        currency: currency
                    }
                })
                .then(function (response) {
                	
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error retrieving fares');
                    }
                });

                return deferred.promise;
            }
        }
    });