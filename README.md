Travel API 
=================

**Please refer [Travel API Architecture](https://bitbucket.org/abhijithonline/klm-travel-api-abhijith/downloads/TravelAPI%20-%20Architecture.pdf) for architecture overview.**

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the project (after starting the application) go to:

[http://localhost:9000/index.html](http://localhost:9000/index.html)

## Technology Stack ##

* Angularjs / HTML5
* Angular Material
* Spring MVC
* Glassfish Jersey OAUTH2
* Java 8